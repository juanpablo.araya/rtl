﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using RTL.TV.API.Controllers;
using RTL.TV.API.Models;
using RTL.TV.API.Services.Abstractions;
using Xunit;

namespace RTL.TV.API.Tests.Controllers
{
    public class ShowsControllerTests
    {
        private readonly ShowsController _controller;
        private readonly Mock<ITVShowService> _tvShowServiceMock;

        public ShowsControllerTests()
        {
            _tvShowServiceMock = new Mock<ITVShowService>();
            _controller = new ShowsController(_tvShowServiceMock.Object);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-10)]
        public async Task Get_IncorrectPage_ShouldReturnError(int page)
        {
            // Arrange

            // Act
            var result = await _controller.Get(page, 100, new CancellationToken());

            // Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-10)]
        public async Task Get_IncorrectPageSize_ShouldReturnError(int pageSize)
        {
            // Act
            var result = await _controller.Get(1, pageSize, new CancellationToken());

            // Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task Get_ShouldReturnValues()
        {
            // Arrange
            var page = 1;
            var pageSize = 100;
            var cancellationToken = new CancellationToken();
            var resultList = new List<TVShow>
            {
                new TVShow(),
                new TVShow()
            };
            _tvShowServiceMock
                .Setup(service => service.GetShows(page, pageSize, cancellationToken))
                .ReturnsAsync(resultList);

            // Act
            var result = await _controller.Get(page, pageSize, cancellationToken);

            // Assert
            Assert.IsType<OkObjectResult>(result);
            Assert.Equal((result as OkObjectResult)?.Value, resultList);
            _tvShowServiceMock.Verify(service => service.GetShows(page, pageSize, cancellationToken));
        }
    }
}