﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using RTL.TV.API.Models;
using RTL.TV.API.Repository.Abstractions;
using RTL.TV.API.Services;
using Xunit;

namespace RTL.TV.API.Tests.Services
{
    public class ScraperServiceTests
    {
        [Fact]
        public void Scrape_ShouldScrapeConfiguredShows()
        {
            // Arrange
            var configuration = new ScraperConfiguration
            {
                ShowIdToScrap = 10,
                ShowsToScrapPerRepetition = 8,
                MaximumShowIdToScrap = 1000
            };
            var cancellationToken = new CancellationToken();
            var scraperRepositoryMock = new Mock<IScraperRepository>();
            scraperRepositoryMock
                .Setup(repository => repository.GetShow(It.IsAny<int>(), It.IsAny<CancellationToken>()))
                .Returns<int, CancellationToken>((id, token) => Task.FromResult(new TVShow {Id = id}));
            var service = new ScraperService(configuration, scraperRepositoryMock.Object);

            // Act
            service.Scrape(cancellationToken);

            foreach (var id in Enumerable.Range(10, 8))
            {
                // Assert: called the repository to get the data of the shows
                scraperRepositoryMock.Verify(repository => repository.GetShow(id, cancellationToken));
                // Assert: called the repository to set the data of the shows
                scraperRepositoryMock.Verify(repository =>
                    repository.SaveShow(It.Is<TVShow>(show => show.Id == id), cancellationToken));
            }

            // Assert: the currentShowId was updated
            Assert.Equal(18, configuration.ShowIdToScrap);
        }

        [Fact]
        public void Scrape_ScraperReturnsNull_ShouldNotSaveIt()
        {
            // Arrange
            var configuration = new ScraperConfiguration
            {
                ShowIdToScrap = 1,
                ShowsToScrapPerRepetition = 1,
                MaximumShowIdToScrap = 1000
            };
            var cancellationToken = new CancellationToken();
            var scraperRepositoryMock = new Mock<IScraperRepository>();
            var service = new ScraperService(configuration, scraperRepositoryMock.Object);

            // Act
            service.Scrape(cancellationToken);

            scraperRepositoryMock.Verify(repository => repository.GetShow(1, cancellationToken));
            scraperRepositoryMock.Verify(repository => repository.SaveShow(It.IsAny<TVShow>(), cancellationToken),
                Times.Never);

            // Assert: currentShowId should be updated, as we indeed tried to get the tvShow 
            Assert.Equal(2, configuration.ShowIdToScrap);
        }
        
        [Fact]
        public void Scrape_MaximumIdReached_ShouldRefreshExistingShow()
        {
            // Arrange
            var configuration = new ScraperConfiguration
            {
                ShowIdToScrap = 1,
                ShowsToScrapPerRepetition = 10,
                MaximumShowIdToScrap = 8
            };
            var cancellationToken = new CancellationToken();
            var scraperRepositoryMock = new Mock<IScraperRepository>();
            var service = new ScraperService(configuration, scraperRepositoryMock.Object);

            // Act
            service.Scrape(cancellationToken);
            
            // Assert: it should always check till Id 8. Under the current configuration, 
            // we expect the service checks Ids [1, 2, 3, 4, 5, 6, 7, 8, 1, 2]
            
            scraperRepositoryMock.Verify(repository => repository.GetShow(1, cancellationToken), Times.Exactly(2));
            scraperRepositoryMock.Verify(repository => repository.GetShow(2, cancellationToken), Times.Exactly(2));
            scraperRepositoryMock.Verify(repository => repository.GetShow(3, cancellationToken), Times.Once);
            scraperRepositoryMock.Verify(repository => repository.GetShow(4, cancellationToken), Times.Once);
            scraperRepositoryMock.Verify(repository => repository.GetShow(5, cancellationToken), Times.Once);
            scraperRepositoryMock.Verify(repository => repository.GetShow(6, cancellationToken), Times.Once);
            scraperRepositoryMock.Verify(repository => repository.GetShow(7, cancellationToken), Times.Once);
            scraperRepositoryMock.Verify(repository => repository.GetShow(8, cancellationToken), Times.Once);
        }
    }
}