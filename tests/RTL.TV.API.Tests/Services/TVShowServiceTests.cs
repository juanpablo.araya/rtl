﻿using System.Threading;
using Moq;
using RTL.TV.API.Repository.Abstractions;
using RTL.TV.API.Services;
using Xunit;

namespace RTL.TV.API.Tests.Services
{
    public class TVShowServiceTests
    {
        [Fact]
        public void Get_ShouldCallRepository()
        {
            // Arrange
            const int page = 1;
            const int pageSize = 10;
            var tvShowRepositoryMock = new Mock<ITVShowRepository>();
            var service = new TVShowService(tvShowRepositoryMock.Object);
            var cancellationToken = new CancellationToken();
            // Act
            service.GetShows(page, pageSize, cancellationToken);

            // Assert
            tvShowRepositoryMock.Verify(repository => repository.Get(page, pageSize, cancellationToken));
        }
    }
}