# RTL - Scraper
The solution contains three projects:

- **RTL.TV.API**: Where the API runs
- **RTL.DbUpdater**: Scripts to initialize the SQL Database
- **RTL.TV.API.Tests**: unit tests

# How to run the API

Open the project and build the solution. The first thing you need to do is to configure the database. 

- Create a database in *MSSQL Server*. For example, **RTL**
- Open *RTL.DbUpdater* and configure the connectionString in *App.config*.
- Configure the connectionString in the *RTL.TV.API* project. The file is *appSettings.json*
- Run the Updater. The application will generate the tables.

Once with the schema created, run *RTL.TV.API*. By default, it will show the *Swagger* tool. It only contains one controller, **Shows**, and one method. Click and execute the method. There you can set the page number and rows per page

During initialization of the API service, there is a background service that gets the data from the TvMaze API. It runs every three minutes. This parameter can be configured in _appsettings.json_

