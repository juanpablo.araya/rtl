﻿using System;
using System.Configuration;
using System.Reflection;
using DbUp;

namespace RTL.DbUpdater
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var connectionString =
                ConfigurationManager.ConnectionStrings["RTL"].ConnectionString;

            var result = DeployChanges.To
                .SqlDatabase(connectionString)
                .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                .LogToConsole()
                .Build()
                .PerformUpgrade();

            if (!result.Successful)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(result.Error);
                Console.ResetColor();
                return;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Success!");
            Console.ResetColor();
        }
    }
}