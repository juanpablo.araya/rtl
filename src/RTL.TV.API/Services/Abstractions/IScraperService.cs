﻿using System.Threading;
using System.Threading.Tasks;

namespace RTL.TV.API.Services.Abstractions
{
    public interface IScraperService
    {
        Task Scrape(CancellationToken cancellationToken);
    }
}