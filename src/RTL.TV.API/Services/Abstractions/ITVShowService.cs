﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using RTL.TV.API.Models;

namespace RTL.TV.API.Services.Abstractions
{
    public interface ITVShowService
    {
        Task<IEnumerable<TVShow>> GetShows(int page, int pageSize, CancellationToken cancellationToken);
    }
}