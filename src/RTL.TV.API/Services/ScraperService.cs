﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using RTL.TV.API.Repository.Abstractions;
using RTL.TV.API.Services.Abstractions;

namespace RTL.TV.API.Services
{
    public class ScraperService : IScraperService
    {
        private readonly ScraperConfiguration _configuration;
        private readonly IScraperRepository _scraperRepository;

        public ScraperService(ScraperConfiguration configuration, IScraperRepository scraperRepository)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _scraperRepository = scraperRepository ?? throw new ArgumentNullException(nameof(scraperRepository));
        }

        public async Task Scrape(CancellationToken cancellationToken)
        {
            foreach (var id in Enumerable.Range(_configuration.ShowIdToScrap, _configuration.ShowsToScrapPerRepetition))
            {
                var filteredId = id % _configuration.MaximumShowIdToScrap;
                if (filteredId == 0)
                {
                    // we also include the upper limit
                    filteredId = _configuration.MaximumShowIdToScrap;
                }
                _configuration.ShowIdToScrap = filteredId;
                var scrapedShow = await _scraperRepository.GetShow(_configuration.ShowIdToScrap, cancellationToken);
                if (scrapedShow == null) continue;
                await _scraperRepository.SaveShow(scrapedShow, cancellationToken);
            }

            // With the id finished, sets the next id to be scraped
            _configuration.ShowIdToScrap++;
        }
    }
}