﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using RTL.TV.API.Models;
using RTL.TV.API.Repository.Abstractions;
using RTL.TV.API.Services.Abstractions;

namespace RTL.TV.API.Services
{
    public class TVShowService : ITVShowService
    {
        private readonly ITVShowRepository _itvShowRepository;

        public TVShowService(ITVShowRepository itvShowRepository)
        {
            _itvShowRepository = itvShowRepository ?? throw new ArgumentNullException(nameof(itvShowRepository));
        }

        public Task<IEnumerable<TVShow>> GetShows(int page, int pageSize, CancellationToken cancellationToken)
        {
            return _itvShowRepository.Get(page, pageSize, cancellationToken);
        }
    }
}