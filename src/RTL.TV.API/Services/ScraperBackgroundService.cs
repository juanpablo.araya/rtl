﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RTL.TV.API.Services.Abstractions;

namespace RTL.TV.API.Services
{
    public class ScraperBackgroundService : BackgroundService
    {
        private readonly ScraperConfiguration _scraperConfiguration;
        private readonly IScraperService _scraperService;
        private readonly ILogger<ScraperBackgroundService> _logger;

        public ScraperBackgroundService(ScraperConfiguration scraperConfiguration, IScraperService scraperService, ILogger<ScraperBackgroundService> logger)
        {
            _scraperConfiguration =
                scraperConfiguration ?? throw new ArgumentNullException(nameof(scraperConfiguration));
            _scraperService = scraperService ?? throw new ArgumentNullException(nameof(scraperService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("[Scraper] Starting");
                var stopWatch = new Stopwatch();
                stopWatch.Start();

                await _scraperService.Scrape(stoppingToken);

                stopWatch.Stop();
                _logger.LogInformation("[Scraper] finished process in {0} ms", stopWatch.ElapsedMilliseconds);
                
                await Task.Delay(_scraperConfiguration.OperationRepetitionInSeconds * 1000, stoppingToken);
            }
        }
    }
}