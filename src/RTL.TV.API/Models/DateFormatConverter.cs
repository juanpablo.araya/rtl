﻿using Newtonsoft.Json.Converters;

namespace RTL.TV.API.Models
{
    public class DateFormatConverter: IsoDateTimeConverter
    {
        public DateFormatConverter()
        {
            DateTimeFormat = "yyyy-MM-dd";
        }
    }
}