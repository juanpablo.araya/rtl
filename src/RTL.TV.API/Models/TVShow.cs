﻿using System.Collections.Generic;

namespace RTL.TV.API.Models
{
    public class TVShow
    {
        public TVShow()
        {
            Cast = new List<Person>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public List<Person> Cast { get; set; }
    }
}