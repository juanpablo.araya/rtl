﻿using System;
using Newtonsoft.Json;

namespace RTL.TV.API.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonConverter(typeof(DateFormatConverter))]
        public DateTime? BirthDay { get; set; }
    }
}