﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RTL.TV.API.Services.Abstractions;

namespace RTL.TV.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ShowsController : Controller
    {
        private readonly ITVShowService _tvShowService;

        public ShowsController(ITVShowService tvShowService)
        {
            _tvShowService = tvShowService ?? throw new ArgumentNullException(nameof(tvShowService));
        }

        [HttpGet]
        public async Task<IActionResult> Get(int page, int pageSize, CancellationToken cancellationToken)
        {
            if (page <= 0) return BadRequest();

            if (pageSize <= 0) return BadRequest();

            return Ok(await _tvShowService.GetShows(page, pageSize, cancellationToken));
        }
    }
}