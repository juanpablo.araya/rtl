﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Newtonsoft.Json.Linq;
using RTL.TV.API.Models;
using RTL.TV.API.Repository.Abstractions;

namespace RTL.TV.API.Repository
{
    public class ScraperRepository : IScraperRepository
    {
        private readonly ScraperConfiguration _configuration;
        private readonly string _connectionString;
        private readonly IHttpClientFactory _httpClientFactory;

        public ScraperRepository(IHttpClientFactory httpClientFactory, ScraperConfiguration configuration,
            string connectionString)
        {
            _httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<TVShow> GetShow(int id, CancellationToken cancellationToken)
        {
            using (var client = _httpClientFactory.CreateClient())
            {
                var request = new HttpRequestMessage(
                    HttpMethod.Get,
                    new Uri(string.Format(_configuration.URL, id)));
                var response = await client.SendAsync(request, cancellationToken);

                if (response.IsSuccessStatusCode)
                {
                    var responseData = await response.Content.ReadAsStringAsync();
                    var json = JObject.Parse(responseData);

                    var tvShow = new TVShow
                    {
                        Id = (int) json["id"],
                        Name = (string) json["name"],
                        Cast = GetCast(json["_embedded"]["cast"] as JArray)
                    };

                    return tvShow;
                }

                return null;
            }
        }

        public async Task SaveShow(TVShow tvShow, CancellationToken cancellationToken)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                const string tvShowSQL = @"
                    MERGE INTO [Show] AS TARGET USING (VALUES(@Id, @Name)) AS SOURCE(Id, Name)
                    ON SOURCE.Id = TARGET.Id
                    WHEN MATCHED THEN 
                        UPDATE SET
                            Name = SOURCE.Name
                    WHEN NOT MATCHED THEN
                        INSERT (Id, Name)
                        VALUES (SOURCE.Id, SOURCE.Name);";
                await connection.ExecuteAsync(new CommandDefinition(tvShowSQL, new {tvShow.Id, tvShow.Name},
                    cancellationToken: cancellationToken));

                var persons = tvShow.Cast.Select(item => new
                {
                    item.Id,
                    item.Name,
                    item.BirthDay
                });
                const string personsSQL = @"
                    MERGE INTO [Person] AS TARGET USING (VALUES(@Id, @Name, @BirthDay)) AS SOURCE(Id, Name, BirthDay)
                    ON SOURCE.Id = TARGET.Id
                    WHEN MATCHED THEN 
                        UPDATE SET
                            Name = SOURCE.Name,
                            BirthDay = SOURCE.BirthDay
                    WHEN NOT MATCHED THEN
                        INSERT (Id, Name, BirthDay)
                        VALUES (SOURCE.Id, SOURCE.Name, SOURCE.BirthDay);";
                await connection.ExecuteAsync(new CommandDefinition(personsSQL, persons,
                    cancellationToken: cancellationToken));

                var cast = tvShow.Cast.Select(item => new
                {
                    ShowId = tvShow.Id,
                    PersonId = item.Id
                });
                const string castSQL = @"
                    MERGE INTO [ShowCast] AS TARGET USING (VALUES(@ShowId, @PersonId)) AS SOURCE(ShowId, PersonId)
                    ON SOURCE.ShowId = TARGET.ShowId AND SOURCE.PersonId = TARGET.PersonId 
                    WHEN NOT MATCHED THEN
                        INSERT (ShowId, PersonId)
                        VALUES (SOURCE.ShowId, SOURCE.PersonId);";
                await connection.ExecuteAsync(new CommandDefinition(castSQL, cast,
                    cancellationToken: cancellationToken));
            }
        }

        private List<Person> GetCast(JArray array)
        {
            return array
                .Select(item => item["person"].ToObject<Person>())
                .ToList();
        }
    }
}