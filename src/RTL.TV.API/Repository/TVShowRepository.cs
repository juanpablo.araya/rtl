﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using RTL.TV.API.Models;
using RTL.TV.API.Repository.Abstractions;

namespace RTL.TV.API.Repository
{
    public class TVShowRepository : ITVShowRepository
    {
        private readonly string _connectionString;

        public TVShowRepository(string connectionString)
        {
            _connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<IEnumerable<TVShow>> Get(int page, int pageSize, CancellationToken cancellationToken)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("pageNumber", page);
                parameters.Add("rowsPerPage", pageSize);
                var shows = (await connection.QueryAsync<TVShow>(new CommandDefinition(@"
                    SELECT * FROM [Show] ORDER BY Id
                    OFFSET ((@pageNumber - 1) * @rowsPerPage) ROWS
                    FETCH NEXT @rowsPerPage ROWS ONLY
                    ",
                    parameters,
                    cancellationToken: cancellationToken))).ToList();

                var showIds = shows.Select(item => item.Id);
                var casts = (await connection.QueryAsync<dynamic>(new CommandDefinition(@"
                    SELECT
                        ShowCast.ShowId, 
                        Person.* 
                    FROM Person 
                    INNER JOIN ShowCast ON ShowCast.PersonId = Person.Id 
                    WHERE ShowCast.ShowId IN @ids 
                    ORDER BY ShowCast.ShowId ASC, Person.BirthDay DESC
                    ",
                    new {ids = showIds},
                    cancellationToken: cancellationToken))).ToList();

                foreach (var item in casts)
                {
                    var show = shows.First(tvShow => tvShow.Id == item.ShowId);
                    show.Cast.Add(new Person
                    {
                        Id = (int) item.Id,
                        Name = (string) item.Name,
                        BirthDay = (DateTime?) item.BirthDay
                    });
                }

                return shows;
            }
        }
    }
}