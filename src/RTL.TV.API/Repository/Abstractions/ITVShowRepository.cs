﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using RTL.TV.API.Models;

namespace RTL.TV.API.Repository.Abstractions
{
    public interface ITVShowRepository
    {
        Task<IEnumerable<TVShow>> Get(int page, int pageSize, CancellationToken cancellationToken);
    }
}