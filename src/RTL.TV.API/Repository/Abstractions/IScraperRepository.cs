﻿using System.Threading;
using System.Threading.Tasks;
using RTL.TV.API.Models;

namespace RTL.TV.API.Repository.Abstractions
{
    public interface IScraperRepository
    {
        Task<TVShow> GetShow(int id, CancellationToken cancellationToken);
        Task SaveShow(TVShow tvShow, CancellationToken cancellationToken);
    }
}