﻿using System.Net.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using RTL.TV.API.Repository;
using RTL.TV.API.Repository.Abstractions;
using RTL.TV.API.Services;
using RTL.TV.API.Services.Abstractions;

namespace RTL.TV.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "RTL TV API", Version = "v1"});
            });
            services.AddLogging(builder =>
            {
                builder.AddConfiguration(Configuration.GetSection("Logging"));
                builder.AddConsole();
                builder.AddDebug();
            });

            var scraperConfiguration = new ScraperConfiguration();
            var connectionString = Configuration.GetConnectionString("RTL");
            Configuration.GetSection(nameof(ScraperConfiguration)).Bind(scraperConfiguration);
            services.AddSingleton(scraperConfiguration);
            services.AddSingleton<IScraperService, ScraperService>();
            services.AddSingleton<IScraperRepository>(provider => new ScraperRepository(
                provider.GetRequiredService<IHttpClientFactory>(),
                provider.GetRequiredService<ScraperConfiguration>(),
                connectionString));
            services.AddHostedService<ScraperBackgroundService>();
            services.AddHttpClient();

            services.AddScoped<ITVShowService, TVShowService>();
            services.AddScoped<ITVShowRepository>(provider => new TVShowRepository(connectionString));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();

            app.UseHttpsRedirection();
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "RTL TV API V1"); });
        }
    }
}