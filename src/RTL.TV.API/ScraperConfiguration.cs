﻿namespace RTL.TV.API
{
    public class ScraperConfiguration
    {
        public ScraperConfiguration()
        {
            ShowIdToScrap = 1;
        }

        public int OperationRepetitionInSeconds { get; set; }
        public int ShowsToScrapPerRepetition { get; set; }
        public int ShowIdToScrap { get; set; }
        public int MaximumShowIdToScrap { get; set; }
        public string URL { get; set; }
    }
}